#!/usr/bin/env python3
# File Name: add-start
# Dependencies: fluxbox or icewm or jwm, desktop_tool, Gtk, pyGtk,
# python os mod, python re mod, python sys mod, yad
# Version: 2.0
# Purpose:  Add startup commands to the icewm / fluxbox / jwm startup
# Authors: Dave

# Copyright (C) Tuesday, Feb. 7, 2011  by Dave / david@daveserver.info
# License: gplv2
# This file is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
################################################################################
#################################################################

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
#from desktop_tool import get_icon as get_icon
import re
import os
import sys 
import gettext
gettext.install("add-start", "/usr/share/locale")

USER_HOME = os.environ['HOME']
DISPLAY = os.environ['DISPLAY']
DISPLAY = re.sub(r':', '', DISPLAY)
DISPLAY_SPLIT = DISPLAY.split('.')
DISPLAY = DISPLAY_SPLIT[0]

with open(USER_HOME+"/.desktop-session/desktop-code."+DISPLAY, "r") as f:
    DESKTOP = f.readline()
    DESKTOP = re.sub(r'\n', '', DESKTOP)
    DESKTOP = re.sub(r'.*-', '', DESKTOP)
    LOCATION=(USER_HOME+"/."+DESKTOP+"/startup")

class Error:
    def __init__(self, error):
        dlg = Gtk.MessageDialog(parent=None, flags=0, message_type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK, text="Error")
        dlg.set_title(_("add-start error"))
        dlg.format_secondary_text(error)
        dlg.set_keep_above(True) # note: set_transient_for() is ineffective!
        dlg.run()
        dlg.destroy()
       
class Success:
    def __init__(self, success):
        dlg = Gtk.MessageDialog(parent=None, flags=0, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Success")
        dlg.set_title(_("Successfully updated"))
        dlg.format_secondary_text(success)
        dlg.set_keep_above(True) # note: set_transient_for() is ineffective!
        dlg.run()
        dlg.destroy()
       
class Remove():
    
    def build_drop_box(self):
        self.RemoveList = Gtk.ListStore(str)
        self.RemoveList.append([_("No line Selected:")])
        
        for line in open(USER_HOME+"/."+DESKTOP+"/startup", "r"):
            if "#" not in line:
                line = re.sub(r'\n', '', line)
                self.RemoveList.append([line])

        self.removeSelect = Gtk.ComboBox.new_with_model(self.RemoveList)
        renderer_text = Gtk.CellRendererText()
        self.removeSelect.pack_start(renderer_text, True)
        self.removeSelect.add_attribute(renderer_text, "text", 0)
        #removeSelect.connect('changed', self.changed_cb)
        self.removeSelect.set_active(0)
        self.selectBox.pack_start(self.removeSelect, False, False, 0)
        self.removeSelect.show()
        
    def updateRemoveSelect(self):
        #remove select box
        self.selectBox.remove(self.removeSelect)
        self.removeSelect.destroy()
        #build drop box
        self.build_drop_box()
    
    
    def apply(self,widget):
        model = self.removeSelect.get_model()
        index = self.removeSelect.get_active()
        line_to_remove = model[index][0]
        line_to_remove = re.sub(r'\(', '\\\(', line_to_remove)
        line_to_remove = re.sub(r'\)', '\\\)', line_to_remove)
        
        for line in open(USER_HOME+"/."+DESKTOP+"/startup", "r"):
            if "%s" % (line_to_remove) not in line:
                text = open((USER_HOME+"/."+DESKTOP+"/startup.new"), "a")
                text.write (line) 
                text.close()
            
        os_system_line="mv "+USER_HOME+"/."+DESKTOP+"/startup.new "+USER_HOME+"/."+DESKTOP+"/startup && chmod 755 "+USER_HOME+"/."+DESKTOP+"/startup"
        os.system(os_system_line)
        Success(_("line has been removed"))
        self.updateRemoveSelect()
                
    def __init__(self):
        
        self.frame = Gtk.Frame()
        self.frame.set_label(_("Remove Items"))
        self.frame.set_border_width(10)
        self.frame.show()
        
        self.vbox = Gtk.VBox()
        self.frame.add(self.vbox)
        self.vbox.show()
        
        label = Gtk.Label()
        label.set_text(_("Line to Remove"))
        self.vbox.pack_start(label, False, False, 0)
        label.show()
        
        self.selectBox = Gtk.VBox()
        self.vbox.pack_start(self.selectBox, False, False, 0)
        self.selectBox.show()
        
        #build drop box
        self.build_drop_box()
        
        self.label = Gtk.Label()
        self.label.set_text(_("Remove"))
        self.label.show()
        
        #BUTTON BOX
        
        buttonbox = Gtk.HButtonBox()
        self.vbox.pack_start(buttonbox, False, False, 0)
        buttonbox.show()
        
        remove = Gtk.Button.new_from_icon_name("gtk-remove", Gtk.IconSize(1))
        #remove.set_label(_("Remove"))
        remove.connect("clicked", self.apply)
        buttonbox.pack_start(remove, False, False, 0)
        remove.show()
        
        close = Gtk.Button.new_from_icon_name("gtk-close", Gtk.IconSize(1))
        #close.set_label(_("Close"))
        close.connect("clicked", lambda w: Gtk.main_quit())
        buttonbox.add(close)
        close.show()
        

       
class Add():
    
    def apply(self, widget):
        command_to_add = self.command.get_text()
        command_to_add = re.sub(r'\(', '\\\(', command_to_add)
        command_to_add = re.sub(r'\)', '\\\)', command_to_add)
        if command_to_add == "": 
            sys.exit(Error(_("You need to enter a command")))
                    
        sleepTime_to_add = int(self.sleepTime.get_value())        
        if sleepTime_to_add != 0:
            prefix="sleep %s && " % (sleepTime_to_add)
        else:
            prefix=""
        
        if DESKTOP == "jwm":
            for line in open(USER_HOME+"/."+DESKTOP+"/startup", "r"):
                if "<StartupCommand>" in line:
                    text = open((USER_HOME+"/."+DESKTOP+"/startup.new"), "a")
                    text.write (line) 
                    text.write (prefix+command_to_add+" &\n")
                    text.close()
                else:
                    text = open((USER_HOME+"/."+DESKTOP+"/startup.new"), "a")
                    text.write (line) 
                    text.close()
        else:
            text = open((USER_HOME+"/."+DESKTOP+"/startup.new"), "a")
            text.write ("#!/bin/bash\n") 
            text.write (prefix+command_to_add+" &\n")
            text.close()
            for line in open(USER_HOME+"/."+DESKTOP+"/startup", "r"):
                if "#!/bin/bash" not in line:
                    text = open((USER_HOME+"/."+DESKTOP+"/startup.new"), "a")
                    text.write (line) 
                    text.close()


        os_system_line="mv "+USER_HOME+"/."+DESKTOP+"/startup.new "+USER_HOME+"/."+DESKTOP+"/startup && chmod 755 "+USER_HOME+"/."+DESKTOP+"/startup "
        os.system(os_system_line)
        Success(_("command has been added"))
        
        #refresh remove drop box
        #Does not work after update, need to close and reopen to refresh.
        Remove.updateRemoveSelect(Remove())
        
    def scale_set_default_values(self, scale):
        #scale.set_update_policy(Gtk.UPDATE_CONTINUOUS)
        scale.set_digits(0)
        #scale.set_value_pos(Gtk.POS_TOP)
        scale.set_draw_value(True)
        
    def __init__(self):
        self.frame = Gtk.Frame()
        self.frame.set_label(_("Add Items"))
        self.frame.set_border_width(10) 
        self.frame.show()
        
        vbox = Gtk.VBox()
        self.frame.add(vbox)
        vbox.show()
        
        label = Gtk.Label()
        label.set_text(_("Command"))
        vbox.pack_start(label, False, False, 0)
        label.show()
        
        self.command = Gtk.Entry()
        self.command.set_text(_("add-start"))
        vbox.pack_start(self.command, False, False, 0)
        self.command.show()
        
        label = Gtk.Label()
        label.set_text(_("Sleep Time"))
        vbox.pack_start(label, False, False, 0)
        label.show()
        
        adj1 = Gtk.Adjustment(value=0, lower=0.0, upper=31.0, step_increment=1.0, page_increment=1.0, page_size=1.0 )
        self.sleepTime = Gtk.HScale()
        self.sleepTime.set_adjustment(adj1)
        self.sleepTime.set_size_request(200, 30)
        self.scale_set_default_values(self.sleepTime)
        vbox.pack_start(self.sleepTime, False, False, 0)
        self.sleepTime.show()
        
        self.label = Gtk.Label()
        self.label.set_text(_("Add"))
        self.label.show()
        
        #BUTTON BOX
        
        buttonbox = Gtk.HButtonBox()
        vbox.pack_start(buttonbox, False, False, 0)
        buttonbox.show()
        
        
        add = Gtk.Button.new_from_icon_name("gtk-add", Gtk.IconSize(1))
        #add.set_label(_("Add"))
        add.connect("clicked", self.apply)
        buttonbox.pack_start(add, False, False, 0)
        add.show()
        
        close = Gtk.Button.new_from_icon_name("gtk-close", Gtk.IconSize(1))
        #close.set_label(_("Close"))
        close.connect("clicked", lambda w: Gtk.main_quit())
        buttonbox.add(close)
        close.show()
        

class mainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_size_request(300,0)
        self.set_border_width(10)
        self.set_title(_("add-start"))
        #pixbuf = get_icon("preferences-system", 48)
        #self.set_icon(pixbuf)
        
        mainbox = Gtk.VBox()
        self.add(mainbox)
        mainbox.show()
        
        label = Gtk.Label()
        label.set_text(_("Changing startup for: "+DESKTOP+"\n"))
        mainbox.pack_start(label, False, False, 0)
        label.show()
        
        self.notebook = Gtk.Notebook()
        self.notebook.set_tab_pos(Gtk.PositionType(2))
        self.notebook.set_size_request(300,200)
        mainbox.pack_start(self.notebook, True, True, 0)
        self.notebook.show()
        
        #Start Add Class
        self.notebook.append_page(Add().frame, Add().label)
        
        #Start Remove Class
        self.notebook.append_page(Remove().frame, Remove().label)
        

if os.path.isfile(LOCATION) == (False):
    sys.exit(Error(_("There is no file ~/.%s/startup \nThe session variable DESKTOP_CODE='%s' \nincorrectly matches your system" % ((DESKTOP), (DESKTOP)) )))
win = mainWindow()
win.connect("delete-event", Gtk.main_quit)
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL) # without this, Ctrl+C from parent term is ineffectual
win.show_all()
Gtk.main()
